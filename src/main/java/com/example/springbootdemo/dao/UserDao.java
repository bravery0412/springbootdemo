package com.example.springbootdemo.dao;

import com.example.springbootdemo.pojo.User;

import java.util.List;

/**
 * @author renyong
 * @create 2017-12-28 9:40
 * @desc
 **/
public interface UserDao {

    public List<User> findUserList(User user);

    public int updateUser(User user);
}
