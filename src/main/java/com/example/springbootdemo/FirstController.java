package com.example.springbootdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author renyong
 * @create 2017-12-26 15:59
 * @desc h
 **/
@RestController
public class FirstController {
    @Autowired
    AppConfig appConfig;

    @RequestMapping("/first")
    public Object first() {
        return appConfig.getUrl();
    }

    @RequestMapping("/doError")
    public Object error() {
        return 1 / 0;
    }

    @RequestMapping("/second")
    @UserAccess(desc = "second")
    public Object second() {
        return "second controller";
    }
}
