package com.example.springbootdemo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author renyong
 * @create 2017-12-27 13:54
 * @desc
 **/
@Component
@ConfigurationProperties(prefix = "connection")
public class AppConfig {

    private String url;
    private String username;
    private String remoteAddress;
    private String password;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "[url=" + url + ", username=" + username + ", remoteAddress=" + remoteAddress + ", password=" + password
                + "]";
    }


}
