package com.example.springbootdemo.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @author renyong
 * @create 2017-12-27 14:23
 * @desc
 **/
@Configuration
public class MainJdbcTemplate {

    @Bean(name = "primaryJdbcTemplate")
    public JdbcTemplate secondaryJdbcTemplate(@Qualifier("primaryDataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
}
