package com.example.springbootdemo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.springbootdemo.dao.UserDao;
import com.example.springbootdemo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author renyong
 * @create 2017-12-27 14:31
 * @desc
 **/
@RestController
public class UserController {

    @Autowired
    @Qualifier("primaryJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserDao userDao;

    /**
     * 测试用户信息
     * @return
     */
    @RequestMapping("/users")
    public String getUseName(){
        String sql="select name from user";
        List<Map<String,Object>> userss=jdbcTemplate.queryForList(sql);
        return  (String)userss.get(0).get("name");
    }
    @RequestMapping(value="/userList",method = { RequestMethod.POST })
    public void getUserList(@RequestBody User user, HttpServletRequest request, HttpServletResponse response){
        response.setCharacterEncoding("utf-8");
        response.setContentType("json/plain;charset=UTF-8");
        JSONObject object = new JSONObject();
        JSONArray  array = new JSONArray();
        int code=1;
        try{
            String sql="select * from user";
            List<Map<String,Object>> userss=jdbcTemplate.queryForList(sql);
            if(userss!=null&&userss.size()>0){
                for(Map<String,Object> entity:userss){
                    array.add(entity);
                }
            }
            object.put("result",array);
            PrintWriter out = response.getWriter();
            out.write(object.toString());

        }catch (Exception e){
            e.printStackTrace();
            object.put("result", "获取用户列表失败");
        }
    }
    @RequestMapping(value="/userLists",method = { RequestMethod.POST })
    public void getUserListByMy(@RequestBody User user, HttpServletRequest request, HttpServletResponse response){
        response.setCharacterEncoding("utf-8");
        response.setContentType("json/plain;charset=UTF-8");
        JSONObject object = new JSONObject();
        JSONArray  array = new JSONArray();
        try {
            List<User> list=userDao.findUserList(user);
            if(list!=null&&list.size()>0){
                for(User entity:list){
                    array.add(entity);
                }
            }
            object.put("result",array);
            PrintWriter out = response.getWriter();
            out.write(object.toString());
        }catch (Exception e){
            e.printStackTrace();
            object.put("result","查询用户失败");
        }

    }

    @RequestMapping(value="/userModify",method = { RequestMethod.POST })
    public void getUserEditor(@RequestBody User user, HttpServletRequest request, HttpServletResponse response){
        response.setCharacterEncoding("utf-8");
        response.setContentType("json/plain;charset=UTF-8");
        JSONObject object = new JSONObject();
        JSONArray  array = new JSONArray();
        try {
            int count=userDao.updateUser(user);
            if(count>0){
                object.put("result","修改成功");
            }
            PrintWriter out = response.getWriter();
            out.write(object.toString());
        }catch (Exception e){
            e.printStackTrace();
            object.put("result","修改失败");
        }

    }



}
